import os 
import random, hangmanart, hangmanwords
word_list = hangmanwords.word_list
random_word = random.choice(word_list)
print(hangmanart.logo)
hangman_display = []
lifes = 7
hangman_counter = 0

def clear_console():
  os.system("clear")

for char in random_word:
  hangman_display.append("_")

while '_' in hangman_display and lifes > 0:
  letter = input("Guess a letter:\n").lower()
  clear_console()
  #print(random_word)
  
  for index, character in enumerate(random_word):
    
    if character == letter:
      
      if hangman_display[index] == letter:
  
        print("You have already chosen that letter, go again")
      hangman_display[index] = character
      print(hangman_display)
  
  if letter not in random_word:
    lifes -= 1
    print(f"Wrong, The letter {letter} is not in the chosen word")
    print(hangmanart.HANGMANPICS[hangman_counter])
    hangman_counter += 1
    print(hangman_display)

if '_' not in hangman_display:
  print(f"You win the chosen word is {''.join(hangman_display)}")

if lifes == 0:
  print(f"You are dead. The correct choice of the word was {random_word}")